Versionsverwaltung
------------------

[Ein Repository](https://de.wikipedia.org/wiki/Repository) (englisch für Lager, Depot oder auch Quelle; deutsch Plural Repositorien), auch – direkt aus dem Lateinischen entlehnt – Repositorium (Pl. Repositorien), ist ein verwaltetes Verzeichnis zur Speicherung und Beschreibung von digitalen Objekten für ein digitales Archiv. Bei den verwalteten Objekten kann es sich beispielsweise um Programme (Software-Repository), Publikationen (Dokumentenserver), Datenmodelle (Metadaten-Repository) oder betriebswirtschaftliche Verfahren handeln. Häufig beinhaltet ein Repository auch Funktionen zur Versionsverwaltung der verwalteten Objekte.

[Git](https://de.wikipedia.org/wiki/Git) ist eine freie Software zur verteilten Versionsverwaltung von Dateien, die durch Linus Torvalds initiiert wurde.

[GitLab](https://de.wikipedia.org/wiki/GitLab) ist eine Webanwendung zur Versionsverwaltung für Softwareprojekte auf Basis von git. Sie bietet diverse Management und Bug-Tracking-Funktionalitäten, sowie mit GitLab CI ein System zur [Kontinuierlichen Integration] https://de.wikipedia.org/wiki/Kontinuierliche_Integration). GitLab ist in der Programmiersprache Ruby entwickelt.

[Markdown](https://de.wikipedia.org/wiki/Markdown) ist eine vereinfachte Auszeichnungssprache deren Ziel es ist, dass schon die Ausgangsform ohne weitere Konvertierung leicht lesbar ist.
