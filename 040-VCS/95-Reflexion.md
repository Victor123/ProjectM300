Reflexion / Dokumentation
-------------------------

[Ein Repository](https://de.wikipedia.org/wiki/Repository) (englisch für Lager, Depot oder auch Quelle; deutsch Plural Repositorien), auch – direkt aus dem Lateinischen entlehnt – Repositorium (Pl. Repositorien), ist ein verwaltetes Verzeichnis zur Speicherung und Beschreibung von digitalen Objekten für ein digitales Archiv. Bei den verwalteten Objekten kann es sich beispielsweise um Programme (Software-Repository), Publikationen (Dokumentenserver), Datenmodelle (Metadaten-Repository) oder betriebswirtschaftliche Verfahren handeln. Häufig beinhaltet ein Repository auch Funktionen zur Versionsverwaltung der verwalteten Objekte.

[Git](https://de.wikipedia.org/wiki/Git) ist eine freie Software zur verteilten Versionsverwaltung von Dateien, die durch Linus Torvalds initiiert wurde.

[GitLab](https://de.wikipedia.org/wiki/GitLab) ist eine Webanwendung zur Versionsverwaltung für Softwareprojekte auf Basis von git. Sie bietet diverse Management und Bug-Tracking-Funktionalitäten, sowie mit GitLab CI ein System zur [Kontinuierlichen Integration] https://de.wikipedia.org/wiki/Kontinuierliche_Integration). GitLab ist in der Programmiersprache Ruby entwickelt.

[Markdown](https://de.wikipedia.org/wiki/Markdown) ist eine vereinfachte Auszeichnungssprache deren Ziel es ist, dass schon die Ausgangsform ohne weitere Konvertierung leicht lesbar ist.

### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Was ist der Unterschied zwischen Git und GitLab?

	 Anhänge: In GitLab kann man Dateien ganz einfach an jedes Issue anhängen. Das ist in GitHub nicht möglich.
	 Issue Tracking: Wenn man GitHub Issues verwendet, wird man hier vielleicht einige Funktionen hier vermissen. GitLab ist da besser aufgestellt und erlaubt den Status und Empfänger für mehrere Issues auf einmal zu ändern.
	 Authentifizierungs-Level: In GitLab kann man Zugangsberechtigungen für verschiedene Team-Mitglieder – je nach Position – modifizieren.  In GitHub kann man entscheiden, ob jemand die Berechtigung zum “Lesen” oder “Verändern” erhält. 
	 In GitLab kann man z. B. eine Berechtigung für den Issue Tracker erteilen ohne Erlaubnis für den Quellcode herauszugeben.

Sowohl GitLab und GitHub sind webbasierte Repositories. Das Ziel von Git ist, Webentwicklung, Webprojekte und Daten, die sich mit der Zeit verändern, zu verwalten.

GitHub
GitHub Projekte können öffentlich gemacht werden und jeder öffentlich geteilte Code kann von jedem genutzt werden. Private Projekte sind in GitHub ebenfalls möglich, erfordern allerdings einen kostenpflichtigen GitHub Account.

Öffentliche Repositories auf GitHub werden häufig verwendet, um Open Source Software zu teilen. GitHub bietet z. B. auch Features wie Issue Tracking und GitHub Pages an.


GitLab
Ähnlich wie GitHub, ist GitLab ein Repository Manager, der Teams erlaubt am Code eines Webprojekts zusammenzuarbeiten. 
GitHub bietet auch GitLab Features für Issue Tracking und Projekt Management. Sie bietet diverse Management und Bug-Tracking-Funktionalitäten, sowie mit GitLab CI ein System zur kontinuierlichen Integration. GitLab ist in den Programmiersprachen Ruby und Go entwickelt.
	 	
	 	
	.......................................................................................


* Was ist TortoiseGit?

	 TortoiseGit ist eine freie grafische Benutzeroberfläche für die Versionsverwaltungs-Software Git unter Windows. Es steht unter der GNU General Public License.
	
    TortoiseGit ist als Shell-Erweiterung implementiert, es integriert sich in den Windows-Explorer und ist daher unabhängig von einer Integrierten Entwicklungsumgebung verwendbar. Es bietet Overlay-Icons im Explorer, die den Status (z. B. versioniert, unverändert, modifiziert, ignoriert) der Dateien integriert in den bestehenden Icons darstellen. Hauptinteraktion erfolgt über das Kontextmenü, über das diverse Aktionen wie Commit, Push oder Pull ausgeführt und TortoiseGit-Dialoge geöffnet werden können.
    Kernaufgaben der Software sind die Versions-, Revisions- und Sourcekontrolle. TortoiseGit basiert technisch auf TortoiseSVN und wurde um Git-spezifische Aspekte erweitert.
    
    Voraussetzung für den Einsatz von TortoiseGit ist ein installiertes Kommandozeilen-Git.

	 	
	 	
	.......................................................................................
	
* Was ist der Unterschied zwischen einer [Versionsverwaltung](https://de.wikipedia.org/wiki/Versionsverwaltung) und dem [Konfigurationsmanagement](https://de.wikipedia.org/wiki/Konfigurationsmanagement)?

	 
	 
	 	
	 	
	.......................................................................................
	
* Welchen Zweck hat die Datei `.gitignore`

	 
	 	
	 	
	.......................................................................................
	


