﻿Praxis: Versionsverwaltung
--------------------------

Da das Know-how wie unsere Infrastruktur im Unternehmen aufgebaut ist einen nicht unerheblichen Wirtschaftlichen Faktor darstellt,
soll dieses in Form eine Versionsverwaltung aufbereitet und gepflegt werden.

### Auftrag

Evaluiert eine Versionsverwaltung und richtet für die Beispiele ein Repository ein.

### Lösungsansatz

Um den Aufwand für das Einrichten einer internen Versionsverwaltung zu vermeiden, verwenden wir [GitLab](http://gitlab.com/).

Dies bietet die Möglichkeit von privaten, nicht öffentlichen Repositories und eine verschlüsselte Kommunikation.

In einem späteren Zeitpunkt können wir das Repository um Kontinuierliche Integration - [continuous integration (CI)](https://de.wikipedia.org/wiki/Kontinuierliche_Integration) erweitern.

### Vorarbeiten

Spielt das [Online Training](https://try.github.io/levels/1/challenges/1) für git durch.

### Vorgehen

* Erstellt auf [GitLab](http://gitlab.com/) einen Account und ein privates Repository, z.B. `i-ch300`. Verwendet die TBZ Mailadresse.


* Fügt über die Weboberfläche eine README.md Datei hinzu und Beschreibt darin den Inhalt des Repositories.


* Installiert und [Tortoise](https://tortoisegit.org/) und [Git](https://git-scm.com/) auf dem lokalen Rechner.


* Clont das erstellte Repository lokal auf den Rechner (Rechte Maustaste im Windows Explorer) mittels des `https` Protokolls.


* Erstellt Verzeichnisse `020-IaC` und `030-Vagrant` im Verzeichnis wo die `README.md` Datei steht und kopiert die Dateien aus den vorherigen Praxisteilen in diese. Vervollständigt diese mit einer `README.md` Datei pro Verzeichnis.

* Der Inhalt sollte ungefähr so aussehen:


	README.md
	020-IaC
	  Vagrantfile
	  README.md
	030-Vagrant
	  Vagrantfile
	  README.md


* Führt die Änderungen im Repository mittels `commit & push` (Rechte Maustaste im Windows Explorer) auf [GitLab](http://gitlab.com/) nach.  


* Gebt das Repository für den Ausbilder mittels Rechten `Reporter` frei und für andere Auszubildende, z.B. für Gruppenarbeiten mit den 
Rechten `Developer`. 

**Nach der Ausbildung und nach Rücksprache mit dem Vorgesetzten kann das Repository freigeschaltet (public) werden und so als Teil der Bewerbungsunterlagen dienen.**

### Lieferobjekt

Ein Privates Repository mit den Dokumentierten Beispielen aus dem Modul.

### Zusatzauftrag (optional)

Clont alle Repositories der Kameraden in ein lokales Stammverzeichnis. Als Input dient ein Export der Klassendatei mit den Ablageorten aus Microsoft Office 365.

Die Klassendatei ist wie folgt aufbereitet und muss im CSV Format gespeichert sein:

	Name      Vorname   Ablageort
	Muster    Hans      git@gitlab.com:hans.muster/M300.git
	...
	
Vorher muss ein SSH Schlüsselpaar erstellt werden, damit ohne Username/Password auf die Repositories zugegriffen werden kann.. Die Anleitung ist auf GibLab unter dem [User Profile](https://gitlab.com/profile/keys) `-> Settings -> SSH Keys -> generate it` nachzulesen.

Die Kameraden müssen vorher Zugriff auf die Repositories gewähren.


