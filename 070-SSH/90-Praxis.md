Praxis
------

### SSH Verbindung einrichten
 
Die Lösung mit eigenen User pro Maschine erlaubt es zwar die User zu identifizieren, ist aber sehr mühsam um schnell von einer Maschine auf die andere zu wechseln, z.B. von web auf db01.

Deshalb wünschen die Anwender eine Möglichkeit um von einem zum anderen System zu wechseln ohne ein Password eingeben zu müssen.
 
#### Auftrag

Vervollständig die User auf der web VM mit ssh-keys.

Verwendet dazu als Vorlage das MultiMachine Beispiel im Verzeichnis `060-TCPIP/mm/` und Erweitert Vagrantfile um die nötigen Befehle. 

#### Lösungsansatz

*bitte Ausfüllen*

#### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `070-SSH` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

#### Lieferobjekte

Vereinfachte Admin Aufgaben, welche Dank SSH-Keys weiter automatisiert werden können.

- - - 

### Public Key auf Gitlab hinterlegen

Statt jedes mal User und Password beim committen von Änderungen auf GibLab anzugeben, soll die Identifizierung via SSH Key erfolgen.

#### Auftrag

Hinterlegt ein für Euch selber erstellen SSH Key auf [GitLab](https://gitlab.com/profile) und Clont Eure Repository neu mit dem SSH Protokoll.

#### Lösungsansatz

*bitte Ausfüllen*

#### Vorgehen

Siehe [SSH](https://gitlab.com/help/ssh/README) Anleitung auf [GitLab](https://gitlab.com/profile) -> SSH Keys -> generate it.

Anschliessend ist das [GitLab](https://gitlab.com/) Repository neu via SSH URL zu clonen. Der SSH URL steht auf der Hauptseite des [GitLab](https://gitlab.com/) Projektes.

**Hinweis:**
Um den SSH Key in [TortoiseGIT](https://tortoisegit.org/) Verwenden zu können muss dieser zuerst via `puttygen` (im Tortoise Programm -> bin Verzeichnis) -> Load -> `%USERPROFILE%\.ssh\id_rsa.pub` in ein internes Putty Format  umgewandelt werden.

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `070-SSH` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

#### Lieferobjekte

Einfacher Zugriff auf Git Repositories.

### Zusatzauftrag

Richtet einen Git Server wie im [Git Buch](https://git-scm.com/book/de/v1/Git-auf-dem-Server-Einrichten-des-Servers) beschrieben ein.



