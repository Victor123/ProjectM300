Reflexion / Dokumentation
-------------------------

Die OpenSSH-Suite ist fester Bestandteil quasi aller Linux-Distributionen.

Diese drei wichtigen Eigenschaften führten zum Erfolg von ssh :
* Authentifizierung der Gegenstelle, kein Ansprechen falscher Ziele
* Verschlüsselung der Datenübertragung, kein Mithören durch Unbefugte
* Datenintegrität, keine Manipulation der übertragenen Daten

Wem die Authentifizierung über Passwörter trotz der Verschlüsselung zu unsicher ist, der benutzt das Public-Key-Verfahren. Hierbei wird asymmetrische Verschlüsselung genutzt, um den Benutzer zu authentifizieren. 

Tunnel bzw. Tunneling bezeichnet in einem Netzwerk die Konvertierung und Übertragung eines Kommunikationsprotokolls, das für den Transport in ein anderes Kommunikationsprotokoll eingebettet wird. 

### Präsentation

![](../../images/Reflexion.png)

### Fragen

Was ist der Unterschied zwischen der id_rsa und id_rsa.pub Datei?

	 
	 	
	 	
	.......................................................................................


Wann darf ein SSH Tunnel nicht angewendet werden?

	 
	 	
	 	
	.......................................................................................

Für was dient die Datei `authorized_keys`?

	 
	 	
	 	
	.......................................................................................
	
Für was dient die Datei `known_hosts`?

	 
	 	
	 	
	.......................................................................................
