Praxis
------

Die Potentiellen Sicherheitslücken sollen konsequent vermindert werden.

### Auftrag

Merzt bekannte Sicherheitslücken aus und erstellt für zukünftige Umgebungen eine Best Practice.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `280-Security` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

Weniger Sicherheitslücken durch konsequente Verwendung von Best Practices.