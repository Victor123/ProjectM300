﻿Praxis
------

Im Rahmen eines Prototypen soll evaluiert werden, ob unsere internen VM's auch in der Cloud lauffähig sind und mit welchen Aufwand.

### Auftrag

Sucht zusammen mit den Fachabteilungen ein bis zwei VM's aus, welche als Test in die Cloud überführt werden können.

### Lösungsansatz

Nach Abklären mit den Fachabteilungen und Berücksichtigung von Sicherheitsaspekten, z.B. welche Daten dürfen in die Cloud, bereiten wir
ein bis zwei VM für die Cloud auf.

### Vorgehen

Erstellt eine Virtuelle Maschine in der Cloud.

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `190-Cloud` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

Ein bis zwei VM die statt lokal in der Cloud verfügbar sein.


	

