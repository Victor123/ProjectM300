Reflexion / Dokumentation
-------------------------

Unter **Cloud Computing** (deutsch Rechnerwolke) versteht man die Ausführung von Programmen, die nicht auf dem lokalen Rechner installiert sind, sondern auf einem anderen Rechner, der aus der Ferne aufgerufen wird (bspw. über das Internet).

Die **Infrastruktur (IaaS)** oder „Cloud Foundation“ stellt die unterste Schicht im Cloud Computing dar. Der Benutzer greift hier auf bestehende Dienste innerhalb des Systems zu, verwaltet seine Recheninstanzen (virtuelle Maschinen) allerdings weitestgehend selbst. 

**Vagrant** unterstützt mehrere Cloud Anbieter und ermöglicht die einfache Erstellung von Virtuellen Maschinen in der Cloud.