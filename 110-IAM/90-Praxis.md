Praxis
------

### Web Server

Bestimmte Verzeichnisse im Web Server sollen nur für Interne Mitarbeiter zugänglich sein.

#### Auftrag

Sichert den Web Server durch Usernamen/Password und dem HTTPS Protokoll ab.

**Zusatzauftrag 1**: Holt Usernamen/Password via [LDAP](https://httpd.apache.org/docs/trunk/howto/auth.html).

**Zusatzauftrag 2**: Installiert ein [X.509-Zertifikat](http://wiki.linuxwall.info/doku.php/en:ressources:dossiers:apache:client_certificate) im Browser und verwendet dieses statt Username/Password.

#### Lösungsansatz

*bitte Ausfüllen*

#### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `110-IAM` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

#### Lieferobjekte

Mit Usernamen/Password abgesicherter Web Server

- - -

### IAM

Ein Identity and Access Management Maturitätsmodell hilft Unternehmen, ihren aktuellen Stand der IAM Implementierung zu bestimmen.

#### Auftrag

Überprüft den aktuellen Stand der IAM Implementierung in Eurem Unternehmen.

#### Lösungsansatz

Überprüfung laut [eCH-0172: IAM-Maturitätsmodell](https://www.ech.ch/vechweb/page?p=dossier&documentNumber=eCH-0172&documentVersion=1.0)

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `110-IAM` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

#### Vorgehen

*bitte Ausfüllen*

#### Lieferobjekte

Ausgefülltes IAM-Maturitätsmodell

