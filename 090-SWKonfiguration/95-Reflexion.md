Reflexion / Dokumentation
-------------------------

`/etc`enthält Konfigurations- und Informationsdateien des Basissystems. Beispiele: fstab, hosts, lsb-release, blkid.tab; hier liegende Konfigurationsdateien können durch gleichnamige Konfigurationsdateien im Homeverzeichnis überlagert werden. Beispiel: bash -> .bashrc

`sed`, `cat` etc. sind Tools zur Bearbeitung von Konfigurationsdateien.

### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Für was kann `cURL` alles Verwendet werden?

	 
	 	
	 	
	.......................................................................................


* Was steht im `/etc/` Verzeichnis?

	 
	 	
	 	
	.......................................................................................


* Was ist der Unterschied zwischen einem Hardlink und einem Symbolischen Link?

	 
	 	
	 	
	.......................................................................................
