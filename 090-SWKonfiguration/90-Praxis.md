Praxis
------

Mittels den Tools `cURL`, `sed` etc. können wir nun die Grundsätze
* Infrastruktur als Code 
* Tests

in unsere Implementierungen einfliessen lassen.

### Aufträge

* Ersetzt mysql <<%EOF% durch cat <<%EOF% und mysql <...
* Versucht den `sed` Befehl in db.sh so zu ändern das MySQL Port nur im Privaten Subnetz 192.168.55.x sichtbar ist.
* Baut nach der Installation und Start von MySQL einen Test des Ports mittels `cURL` ein.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `090-SWKonfiguration` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

*bitte Ausfüllen*