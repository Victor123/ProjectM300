Reflexion / Dokumentation
-------------------------

Service Discovery ist der Prozess, Clients eines Service mit Verbindungsinformationen (normalerweise IP-Adresse und
Port) einer passenden Instanz davon zu versorgen.

Ein [Rechnerverbund oder Computercluster](https://de.wikipedia.org/wiki/Rechnerverbund), meist einfach Cluster genannt (vom Englischen für „Rechner-Schwarm“, „-Gruppe“ oder „-Haufen“), bezeichnet eine Anzahl von vernetzten Computern. 

[Swarm](https://docs.docker.com/swarm)  ist  das  native  Clustering-Tool  von  Docker.

### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Welchen Mehrwert bietet `swarm`?

	 
	 	
	 	
	.......................................................................................


* Wo wird Service Discovery und Lastverteilung angewendet?

	 
	 	
	 	
	.......................................................................................
	

* Wozu braucht es Cluster?

	 
	 	
	 	
	.......................................................................................
