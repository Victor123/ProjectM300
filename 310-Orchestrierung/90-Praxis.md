Praxis
------

Die Antwortzeiten via Web Server sind zu langsam und deshalb sollen mehrere Instanzen erstellt werden. Von MySQL wird nur eine Instanz benötigt.

### Auftrag

Erstellt 3 Instanzen (replicas) vom Web Server und eine MySQL Instanz welche in der `swarm` Umgebung laufen.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `310-Orchestrierung` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

Schnellere Antwortzeiten vom Web Server durch n:1 Web Server zu MySQL Instanzen.