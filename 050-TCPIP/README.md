Netzwerke und Ports
-------------------

Transmission Control Protocol / Internet Protocol (TCP/IP) ist eine Familie von Netzwerkprotokollen und wird wegen ihrer großen Bedeutung für das Internet auch als Internetprotokollfamilie bezeichnet.

Ursprünglich wurde TCP als monolithisches Netzwerkprotokoll entwickelt, jedoch später in die Protokolle IP und TCP aufgeteilt. Die Kerngruppe der Protokollfamilie wird durch das User Datagram Protocol (UDP) als weiteres Transportprotokoll ergänzt. Außerdem gibt es zahlreiche Hilfs- und Anwendungsprotokolle, wie zum Beispiel [DHCP](http://de.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) und [ARP](http://de.wikipedia.org/wiki/Address_Resolution_Protocol).

Die Identifizierung der am Netzwerk teilnehmenden Geräte geschieht über [IP-Adressen](http://de.wikipedia.org/wiki/IP-Adresse) (z.B. 10.10.32.1).

### Beispiele

* [mm](mm/README.md) - Multi Maschine Umgebung mit fixen IP-Adressen

 
