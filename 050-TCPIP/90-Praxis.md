﻿Praxis
------

In einem separaten Subnetz soll zu Testzwecken, ein Web Server mit Datenbank eingerichtet werden.

Der Web Server soll gegen aussen sichtbar sein, die Datenbank nur innerhalb des Subnetzes.

### Auftrag

Richtet zwei Server ein. Einen mit einem Web Server und einen mit einer Datenbank.

Überprüft die TCP/IP Adressen und die offenen Ports mit einem Portscanner, z.B. nmap.

Stellt den Inhalt des Web Server, zu Demozwecken via Internet zur Verfügung.

### Lösungsansatz

Um eine grössere Flexibilität zu gewährleisten richten wir zwei Server ein. Einer mit Apache und einer mit MySQL.

Um eine Kommunikation Apache Server - MySQL zu ermöglichen, aber keinen offenen MySQL Port nach draussen zu haben, richten wir ein 
separates Subnetzwerk für Apache und MySQL ein.

Der Apache Server ist der einzige wo von extern (via NAT) erreichbar ist.

### Vorgehen

Erstellt die VM's für das [MultiMachine Beispiel](mm/README.md).


Überprüft die IP-Adressen und offenen Ports des Beispiels, mittels nmap.

* In der web VM


	vagrant ssh web 
	nmap -T4 -F 192.168.55.*
	
* Auf dem lokalen Rechner


	nmap -T4 -F -Pn localhost


Versucht von der web VM Euch mit der MySQL Datenbank zu verbinden.


	mysql -uroot -padmin -hdb01


Beantwortet die Fragen in Reflexion.


**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `050-TCPIP` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

**Hinweis**: Um nmap in cygwin verfügbar zu machen, ist es am einfachsten die ZIP Datei herunterzuladen und der entpackte Inhalt nach `<cygwin>/usr/local/bin` zu kopieren.

#### Fortgeschrittene

Installiert in der web VM zusätzlich das MySQL UserInterface [Adminer](https://www.adminer.org/).

Das User Interface soll via [http://localhost:8080/adminer.php](http://localhost:8080/adminer.php) mit User/Password: root/admin erreichbar sein.

Beim Anmelden an das User Interface muss beim Server `db01` angegeben werden.


### Lieferobjekte

Ein Web und ein Datenbank Server in einem privaten Subnetz.
