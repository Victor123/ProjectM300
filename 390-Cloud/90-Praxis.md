Praxis
------

Für einen Test soll die interne Docker Umgebung mit Cloud Umgebungen verglichen werden.
 
### Auftrag

Installiert 1 - 2 Docker Images in der Cloud

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `390-Cloud` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

1 - 2 Docker Images welche in der Cloud laufen.