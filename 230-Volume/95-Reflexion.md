﻿Reflexion / Dokumentation
-------------------------

Bis jetzt gingen alle Änderungen im Dateisystem beim Löschen des Docker Containers komplett verloren.

Um Daten Persistent zu halten stellt Docker verschiedene Varianten zur Verfügung

* Ablegen der Daten auf dem Host

* Sharen der Daten zwischen Container

* Eigene, sogenannte Volumes, zum Ablegen von Daten


### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Was befinden sich die MySQL Daten wenn der MySQL Container zerstört wurde?

	 
	 	
	 	
	.......................................................................................

* Was ist der Unterschied zwischen Data Containern und Named Volumes?

	 
	 	
	 	
	.......................................................................................

* Wo befinden sich die Daten des Web Servers?

	 
	 	
	 	
	.......................................................................................
	



