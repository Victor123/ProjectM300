﻿Volumes
-------

Bis jetzt gingen alle Änderungen im Dateisystem beim Löschen des Docker Containers komplett verloren.

Um Daten Persistent zu halten stellt Docker verschiedene Varianten zur Verfügung

* Ablegen der Daten auf dem Host

* Sharen der Daten zwischen Container

* Eigene, sogenannte Volumes, zum Ablegen von Daten


### Beispiele


