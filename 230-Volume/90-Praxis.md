Praxis
------

Die Daten des MySQL Server und des Web Servers sollen nicht mehr verloren gehen.


### Auftrag

Speichert die Daten des MySQL Servers in einem Named Volume und die Daten des Web Servers auf dem Host.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `230-Volume` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

Persistente Daten.