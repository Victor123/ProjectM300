﻿Praxis
------

Als Umfangreicherer Prototype wollen wir das REST Beispiel aus dem Kapitel 080-Services nach zwei Docker Images umbauen.

### Auftrag

Baut das Beispiel aus Kapitel 080-Services nach Docker um.

Das REST Beispiel soll dabei in einem eigenen Netzwerk laufen.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `220-Network` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

Zwei Docker Images welche gleich funktionieren wie das 080-Services Beispiel aber in Docker abgebildet sind.
