﻿Reflexion / Dokumentation
-------------------------

Mittels Ports können Container mit der Aussenwelt verbunden werden.


Container können mittels folgenden Methoden untereinander kommunizieren:

* Linken (alt)

* Docker container networking (neu)


### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Was passiert mit den Daten in der MySQL Datenbank wenn der Container gelöscht wird?

	 
	 	
	 	
	.......................................................................................

* Was passiert mit der Installierten Software aus dem Container verlinken Beispiel?

	 
	 	
	 	
	.......................................................................................

* Wie können wir die SQL Freigabe Befehle automatisieren?

	 
	 	
	 	
	.......................................................................................
	
* Wie können wir die Installierte Software aus dem Container verlinken Beispiel persistieren?

	 
	 	
	 	
	.......................................................................................