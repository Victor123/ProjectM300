Ubuntu/Docker mit MySQL
-----------------------

Ubuntu/Docker VM mit MySQL Docker Image und den MySQL Client installiert auf dem Host.

Image builden:

	cd /vagrant/mysql
	docker build -t mysql .
	
starten:

	docker run --rm -d -p 3306:3306 --name mysql mysql

Funktionsfähigkeit überprüfen:

	docker exec -it mysql bash
	
und im Container

	ps -ef
	netstat -tulpen
	exit

MySQL Client im Container starten und Zugriff via Host freischalten:

	docker exec -it mysql mysql -uroot -pS3cr3tp4ssw0rd

	CREATE USER 'root'@'%' IDENTIFIED BY 'admin';
	GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
	FLUSH PRIVILEGES;

MySQL Client auf dem Host starten:

	mysql -uroot -padmin -h127.0.0.1
	