Services, REST und curl
-----------------------

Der [Apache HTTP Server](http://de.wikipedia.org/wiki/Apache_HTTP_Server) ist ein quelloffenes und freies Produkt der Apache Software Foundation und der meistbenutzte Webserver im Internet.

[Representational State Transfer (abgekürzt REST, seltener auch ReST)](http://de.wikipedia.org/wiki/Representational_State_Transfer) bezeichnet ein Programmierparadigma für verteilte Systeme. REST ist eine Abstraktion der Struktur und des Verhaltens des World Wide Web. REST fordert, dass eine Web-Adresse (URI) genau einen Seiteninhalt repräsentiert, und dass ein Web-/REST-Server auf mehrfache Anfragen mit demselben URI auch mit demselben Webseiteninhalt antwortet.

Das [Common Gateway Interface (CGI)](http://de.wikipedia.org/wiki/Common_Gateway_Interface) ist ein Standard für den Datenaustausch zwischen einem Webserver und dritter Software, die Anfragen bearbeitet. CGI ist eine schon länger bestehende Variante, Webseiten dynamisch bzw. interaktiv zu machen, deren erste Überlegungen auf das Jahr 1993 zurückgehen.

Durch das Zusammenspiel von REST und CGI kann ein HTTP Server dynamisch (HTTP GET) Inhalte anzeigen und Daten mittels HTTP POST, PUT oder DELETE empfangen. Die Methoden GET, POST, PUT und DELETE entsprechen dem REST Paradigma.

### Beispiele

