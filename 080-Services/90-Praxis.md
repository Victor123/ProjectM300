﻿Praxis
------

Auf dem Weg zum automatisierten Gebäude sollen zuerst Daten über Temperatur, Lichteinfall, Öffnen und Schliessen der Türen und Fenster 
in einer einfachen Datenbank gesammelt werden. 

### Auftrag

Implementiert einen einfachen Ansatz um Sensordaten in eine Datenbank zu schreiben.

Neben dem automatisierten Ansatz via Sensor (Internet der Dinge) sollen die Daten auch Manuell via HTML Seite erfasst werden können.

### Lösungsansatz

Einrichten eines Web Servers und einer Datenbank. 

Auf dem Web Server wird ein einfaches CGI-Script eingerichtet welches Daten via HTTP POST entgegennimmt und in die Datenbank schreibt.

### Vorgehen

* Portiert das Beispiel vom Rasperrry Pi in das [MultiMachine Beispiel](mm/README.md).


* Teilt Datenbank und Web Server in die jeweiligen Scripts auf.

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `080-Services` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

Einfache Datenerhebung von Sensordaten via REST (automatisiert) oder mittels einer HTML Seite (manuell).
