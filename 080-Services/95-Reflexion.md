Reflexion / Dokumentation
-------------------------

Mittels einer Kombination von Linux (Ubuntu basiert auf Linux, bzw. Debian), dem Web-Server Apache und der Relationalen Datenbank MySQL bauten wir eine Datenablageplattform für Sensordaten.

Dazu haben wir verwendet:
* Das Hypertext Transfer Protocol (HTTP, englisch für Hypertext-Übertragungsprotokoll) ist ein **Protokoll zur Übertragung von Nachrichten und Daten**. Es wird hauptsächlich eingesetzt, um Webseiten (Hypertext-Dokumente) aus dem World Wide Web (WWW) in einen Webbrowser zu laden. Es ist jedoch nicht prinzipiell darauf beschränkt und auch als allgemeines Dateiübertragungsprotokoll sehr verbreitet.
* [Representational State Transfer (abgekürzt REST, seltener auch ReST)](http://de.wikipedia.org/wiki/Representational_State_Transfer) bezeichnet ein Programmierparadigma für verteilte Systeme. REST ist eine Abstraktion der Struktur und des Verhaltens des World Wide Web. REST fordert, dass eine Web-Adresse genau einen Seiteninhalt repräsentiert, und dass ein Web-/REST-Server auf mehrfache Anfragen mit demselben URI auch mit demselben Webseiteninhalt antwortet.
* Das [Common Gateway Interface (CGI)](http://de.wikipedia.org/wiki/Common_Gateway_Interface) ist ein Standard für den Datenaustausch zwischen einem Webserver und dritter Software, die Anfragen bearbeitet. CGI ist eine schon länger bestehende Variante, Webseiten dynamisch bzw. interaktiv zu machen, deren erste Überlegungen auf das Jahr 1993 zurückgehen.
* [MySQL](http://de.wikipedia.org/wiki/MySQL) ist eines der weltweit verbreitetsten relationalen Datenbankverwaltungssysteme. Es ist als Open-Source-Software sowie als kommerzielle Enterprise Version für verschiedene Betriebssysteme verfügbar und bildet die Grundlage für viele dynamische Webauftritte.

Durch das Zusammenspiel von REST und CGI kann ein HTTP Server dynamisch (HTTP GET) Inhalte anzeigen und Daten mittels HTTP POST, PUT oder DELETE empfangen. Die Methoden GET, POST, PUT und DELETE entsprechen dem REST Paradigma.

### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Was ist der Unterschied zwischen einem API und einem Paradigma?

	 
	 	
	 	
	.......................................................................................

* Was ist REST, ein API oder ein Paradigma?

	 
	 	
	 	
	.......................................................................................
	
* Was ermöglicht die Kombination CGI und Bash?


	 
	 	
	 	
	.......................................................................................