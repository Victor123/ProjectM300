﻿Praxis
------

Um eine grössere Sicherheit zu gewährleisten sollen alle Server mit einer eigenen Firewall geschützt werden.

Die Firewall wird nach dem [White Liste](https://de.wikipedia.org/wiki/Wei%C3%9Fe_Liste) Ansatz geführt, d.h. zuerst werden
alle Ports geschlossen und nur vertrauenswürde wieder geöffnet.

Zusätzlich soll der `master` nur noch via dem `web` Server erreichbar sein, z.B. als http://localhost:8080/master/...

### Auftrag

Installiert auf jedem Server eine Firewall nach dem [White Liste](https://de.wikipedia.org/wiki/Wei%C3%9Fe_Liste) Ansatz.

Deaktiviert alle Port Weiterleitungen auf dem `master`, sodass dieser nur noch via `web` Server erreichbar ist.

### Lösungsansatz

Für ein einfaches und automatisiertes Vorgehen wird [ufw](http://wiki.ubuntuusers.de/ufw) eingesetzt. 

Als Reverse Proxy der Apache Server.

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `120-Firewall` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**


### Lieferobjekte

Alle Server sind mit einer Firewall Software ausgerüstet. 

Es sind so wenig Ports geöffnet wie nötig und alle Server Software (REST, adminer, phpldapadmin) nur noch via dem `web` Server erreichbar.
