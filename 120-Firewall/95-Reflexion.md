Reflexion / Dokumentation
-------------------------

Eine [Firewall](http://de.wikipedia.org/wiki/Firewall) (von englisch firewall ‚Brandwand‘ oder ‚Brandmauer‘) ist ein Sicherungssystem, das ein Rechnernetz oder einen einzelnen Computer vor unerwünschten Netzwerkzugriffen schützt und ist weiter gefasst auch ein Teilaspekt eines Sicherheitskonzepts.

Der [Reverse Proxy](http://de.wikipedia.org/wiki/Reverse_Proxy) ist ein Proxy, der Ressourcen für einen Client von einem oder mehreren Servern holt. Die Adressumsetzung wird in der entgegengesetzten Richtung vorgenommen, wodurch die wahre Adresse des Zielsystems dem Client verborgen bleibt. Während ein typischer Proxy dafür verwendet werden kann, mehreren Clients eines internen (privaten – in sich geschlossenen) Netzes den Zugriff auf ein externes Netz zu gewähren, funktioniert ein Reverse Proxy genau andersherum.


### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Das ist der Unterschied zwischen einem Web Server und einen Reverse Proxy?

	 
	 	
	 	
	.......................................................................................

* Was verstehen wir unter einer "White List"?

	 
	 	
	 	
	.......................................................................................
	
* Was wäre die Alternative zum Absichern der einzelnen Server mit einer Firewall?

	 
	 	
	 	
	.......................................................................................