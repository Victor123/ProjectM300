Reflexion / Dokumentation
-------------------------

[Vagrant](https://de.wikipedia.org/wiki/Vagrant_(Software)) ist eine Open-Source Ruby-Anwendung zum Erstellen und Verwalten von virtuellen Maschinen.

[Bash (für Bourne-again shell)](https://de.wikipedia.org/wiki/Bash_(Shell)) ist eine freie Unix-Shell und Teil des GNU-Projekts. Sie ist heute auf vielen unixoiden Systemen die Standard-Shell.

Das [Advanced Packaging Tool (APT)](http://de.wikipedia.org/wiki/Advanced_Packaging_Tool) ist ein Paketverwaltungssystem, das im Bereich des Betriebssystems Debian GNU/Linux entstanden ist und [dpkg](http://de.wikipedia.org/wiki/Debian_Package_Manager) zur eigentlichen Paketverwaltung benutzt.

### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Für was steht "Infrastruktur als Code"?

	 
	 	
	 	
	.......................................................................................

* Für was steht "Infrastructure as a Service - IaaS"?

	 
	 	
	 	
	.......................................................................................
	
* Welche Funktionen erfüllen Tools?

	 
	 	
	 	
	.......................................................................................	
	
* Was fehlt, Ausgehend von unseren Grundsätzen?

	 
	 	
	 	
	.......................................................................................

