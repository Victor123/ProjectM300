﻿Praxis
------

Im Rahmen eines internen Prototypen soll abgeklärt werden, welche interne Dienste, wie Blogs, Content Management Systeme, Datenbank
Virtualisiert werden können.

Damit soll eine grössere Flexibilität beim Einsatz der Hardware und Dienste erreicht werden.

### Auftrag

Analysiert die internen eingesetzten Dienste und überprüft welche Virtualisiert werden können. 

Gleichzeitig sollen Alternative Produkte evaluiert werden.

Die Prototypen sollen einfach in die Produktion überführt werden können.

### Vorarbeiten

Um ein wenig Gefühl mit der [Bash](https://de.wikipedia.org/wiki/Bash_(Shell)) und Vagrant zu bekommen, ist es ratsam 
* Das Tutorial [Learning the Shell](http://linuxcommand.org/lc3_learning_the_shell.php) durchzuarbeiten.
* Den Workflow mit den Beispielen aus `030-Vagrant` und `050-TCPIP` durchzuspielen. 

### Lösungsansatz

Analysieren der intern eingesetzten Dienste zusammen mit den Fachabteilungen und Auswahl von 2 – 3 für einen Prototypen.

Wahl des "Infrastructure as Code" Ansatzes für die einfache Überführung des Prototypen in die Produktion.

### Vorgehen

#### 1. Vagrant Provisioning

Installiert eine Datenbank wie z.B. MySQL mit UserInterface [Adminer](https://www.adminer.org/).

	sudo apt-get update
	sudo apt-get -y install debconf-utils apache2
	sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password admin'
	sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password admin'
	sudo apt-get -y install php libapache2-mod-php php-curl php-cli php-mysql php-gd mysql-client mysql-server 
	# Admininer SQL UI 
	sudo mkdir /usr/share/adminer
	sudo wget "http://www.adminer.org/latest.php" -O /usr/share/adminer/latest.php
	sudo ln -s /usr/share/adminer/latest.php /usr/share/adminer/adminer.php
	echo "Alias /adminer.php /usr/share/adminer/adminer.php" | sudo tee /etc/apache2/conf-available/adminer.conf
	sudo a2enconf adminer.conf 
	sudo service apache2 restart  

Testet die Befehle vorher in der VM. `vagrant ssh` und dann Befehle eingeben.

Um Änderungen im Vagrantfile zu aktivieren ist `vagrant provision` zu verwenden. Oder besser `vagrant destroy -f && vagrant up`

Das User Interface ist via [http://localhost:8080/adminer.php](http://localhost:8080/adminer.php) mit User/Password: root/admin erreichbar.

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `030-Vagrant` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie.**

#### 2. Standard Vagrant Boxes (optional)

Installiert verschiedene Vagrant Boxes für Populäre Software wie

- [WordPress](https://atlas.hashicorp.com/boxes/search?utf8=%E2%9C%93&sort=&provider=virtualbox&q=wordpress) - Blog
- [Typo3](https://atlas.hashicorp.com/boxes/search?utf8=%E2%9C%93&sort=&provider=virtualbox&q=typo3) - Content Management System (CMS)
- Die [IoTKit](https://github.com/mc-b/IoTKit) Entwicklungsumgebung wie unter Hinweise beschrieben.

#### 3. Mehrere VM's (optional)

Erstellt mehrere VM's abgeleitet aus dem Beispiel von `020-IaC/web` mit jeweils anderer Startseite (index.html).

Fasst die Befehle in einem Bash Script zusammen.

### Lieferobjekt

2 - 3 Dienste welche Virtualisiert und in beliebiger Anzahl Produziert werden können.

### Hinweise

Im Verzeichnis `iot` befindet sich eine IoTKit Umgebung mit Software für ARM mbed Entwicklungsumgebung (Atom, Arduino), Leiterplatinen Herstellung (EAGLE, Fritzing) und 3D Software (FreeCAD).

Umgebung via `vagrant up && vagrant reload` (geht min. 10 Minuten) initialisieren, neu starten und folgende Beispiele anschauen:


* Atom Editor öffnen und mittels `Open Project` im `~/ws/gpio/DigitalOut` Projekt öffnen und mittels > (erstes Icon links) compilieren


* EAGLE starten und IoTKit Board im Ordner `~/ws/IoTKit/IoTKitShield/V2.0` öffnen.


* FreeCAD starten und z.B. Roboterarm Teile im Ordner `~/ws/IoTKit/CAD` öffnen.
