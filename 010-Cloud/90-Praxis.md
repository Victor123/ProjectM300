﻿Praxis 
------

Ein Entwickler muss für das "Internet der Dinge" einen Prototype entwickeln. Die dazugehörenden Tools laufen jedoch nur auf Linux.

### Auftrag

Aufsetzen einer Entwicklungsmaschine für das "Internet der Dinge" mit Ubuntu Desktop Linux, wo parallel neben der bestehenden
Windows Installation "on demand" betrieben werden kann. 

### Lösungsansatz

Um Parallel zu Arbeiten wird eine Virtualisierung-Umgebung, statt Dual Boot, eingesetzt.

Um, ausser der Arbeitsleitung, keine Zusätzlichen Kosten durch Lizenzgebühren zu erzeugen, wird die Open Source Virtualisierung-Umgebung
[VirtualBox](https://www.virtualbox.org/) eingesetzt. Ubuntu Desktop Linux ist ebenfalls Open Source weshalb auch dort keine Lizenzgebühren anfallen.

### Vorgehen

* Installieren der Virtualisierung-Umgebung [VirtualBox](https://www.virtualbox.org/) auf dem Rechner.

* [Ubuntu Desktop](https://www.ubuntu.com/download/desktop) Image herunterladen - **wenn ein interner Server vorhanden ist**: {{config.server}}/cd-mirror verwenden.
  
* Erstellen einer VirtualBox VM mit Linux und dem heruntergeladenen Image als CD Laufwerk.

* Booten der VM und Installieren der Linux Desktop Umgebung.
 
* Im Menu das [Software Controlcenter](https://wiki.ubuntuusers.de/Synaptic/) ö.ä. suchen und starten.

* Im  [Software Controlcenter](https://wiki.ubuntuusers.de/Synaptic/) nach apache suchen und diesen installieren.

* Testen mittels Starten des Browsers und Angabe von [http://localhost](http://localhost).

### Lieferobjekt

Entwicklungsmaschine für das "Internet der Dinge" mit Ubuntu Desktop Linux, welche parallel neben Windows verwendet werden kann.

**Hinweis** die Formulierung hat Kundenzentriert (Vergleiche Digitalisierung!) zu erfolgen!
