Vagrant Image mit Docker
------------------------

Einfache VM mit Docker installiert.

Der Standarduser `vagrant` ist in der docker Gruppe eingetragen.

Docker ist via Provisioner installiert.

Die VM kann mittels folgenden Befehlen, für weitere Übungen verwendet werden:

	vagrant up
	vagrant ssh


	sudo -i
	cd /vagrant
	bash -x base.sh
	bash -x vagrant.sh
	usermod -aG docker vagrant
	exit


	vagrant package --base vdocker_default_...
	vagrant box add package.box --name ubuntu/xenial64-docker

Die Details stehen im Kapitel [Packer Alternativen](../180-Images/20-Alternativen.md)

Um eine neue VM, ab dieser zu Erstellen folgendes eingeben:

	mkdir test
	cd test
	vagrant init ubuntu/xenial64-docker
	vagrant up

