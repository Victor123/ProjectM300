﻿Reflexion / Dokumentation
-------------------------

Container ändern die Art und Weise, wie wir Software entwickeln, verteilen und laufen lassen, grundlegend.

Entwickler können Software lokal bauen, die woanders genauso laufen wird – sei es ein Rack in der IT Abteilung,
der Laptop eines Anwenders oder ein Cluster in der Cloud. 

Administratoren können sich auf die Netzwerke, Ressourcen und die Uptime konzentrieren und müssen weniger Zeit mit dem Konfigurieren von Umgebungen und dem Kampf mit Systemabhängigkeiten verbringen.


### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Was ist der Unterschied zwischen Vagrant und Docker?

	 
	 	
	 	
	.......................................................................................

* Was welches Tools aus dem Docker Universum ist Vergleichbar mit Vagrant?

	 
	 	
	 	
	.......................................................................................
	
* Was macht der Docker Provisioner ?

	 
	 	
	 	
	.......................................................................................
