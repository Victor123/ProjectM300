﻿Praxis
------

Um die Dienste flexibler und schlanker gestalten, evaluieren wir im Rahmen eines Prototyps welche VM auch als Docker Container
betrieben werden können.

Dazu bauen wir zuerst eine Docker Umgebung in einer VM auf.

### Auftrag

Erstellt eine VM mit Docker.

### Lösungsansatz

Statt `docker machine` zu verwenden haben wir uns entschlossen selber eine VM mit Docker aufzusetzen.

Wir erwarten so eine grössere Flexibilität beim Einsatz von Docker.

Wir verwenden Ubuntu 14.04 als Basisimage, weil dort der Lerneffekt grösser ist.

### Vorgehen

* Erstellt eine neue VM mit Vagrant, welches ein Standard Ubuntu und Docker beinhaltet, anhand nachfolgender Anleitungen:


  * [Get Docker for Ubuntu](https://docs.docker.com/engine/installation/linux/ubuntu/)
  * [Post-installation steps for Linux](https://docs.docker.com/engine/installation/linux/linux-postinstall/)
  

* Überprüft die Funktionsweise mittels folgender Anleitung:

   * [Verify your installation](https://docs.docker.com/engine/getstarted/step_one/#/step-3-verify-your-installation)
    
**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `200-Container` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte    

VM mit Docker für Prototyping.
