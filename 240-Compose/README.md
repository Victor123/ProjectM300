Docker Compose
--------------

[Docker Compose](https://docs.docker.com/compose/overview/) ist dazu gedacht, Docker-Umgebungen schneller erstellen zu können. 

[YAML](https://de.wikipedia.org/wiki/YAML) ist eine vereinfachte Auszeichnungssprache (englisch markup language) zur Datenserialisierung, angelehnt an XML (ursprünglich) und an die Datenstrukturen in den Sprachen Perl, Python und C sowie dem in [RFC 2822](https://tools.ietf.org/html/rfc2822) vorgestellten E-Mail-Format.



### Beispiele

