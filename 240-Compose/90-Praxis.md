Praxis
------

Um das Handling unserer Container zu vereinfachen soll diese von der CLI nach docker-compose überführt werden.

### Auftrag

Erstellt eine `docker-compose.yml` welche unsere Container mit allen Argumenten (Ports, Volumes etc.) zusammenfasst.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `240-Compose` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**


### Lieferobjekte

Konfigurationsdatei zum einfachen Starten unserer Umgebung.

