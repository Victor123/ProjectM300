﻿Reflexion / Dokumentation
-------------------------

Docker Datacenter bietet eine integrierte Plattform für Entwickler und IT-Operatoren für das Betreiben von Unternehmenssoftware.

Docker Datacenter bringt Sicherheit, Richtlinien und Kontrollen in den Anwendungslebenszyklus, ohne dabei auf Flexibilität oder Anwendung-Portabilität zu verzichten. 

Docker Datacenter integriert sich in Ihr Unternehmen - vom Netzwerk, offenen APIs und Schnittstellen bis hin zur Flexibilität, um eine Vielzahl von Workflows zu unterstützen.

### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Was bringt ein Data Center?

	 
	 	
	 	
	.......................................................................................

* Was sind die Alternativen?

	 
	 	
	 	
	.......................................................................................
	
* Wo kann ein Data Center Betrieben werden?

	 
	 	
	 	
	.......................................................................................
	