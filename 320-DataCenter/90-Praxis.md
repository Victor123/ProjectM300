Praxis
------

Um die Arbeitsabläufe weiter zu automatisieren sollen verschiedene Data Center Lösungen geprüft werden.

### Auftrag

Setzt das Docker Datacenter zu Testzwecken auf.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `310-Orchestrierung` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**


### Lieferobjekte