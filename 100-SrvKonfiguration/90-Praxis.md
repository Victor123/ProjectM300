Praxis
------

Statt nur am Anfang zu Testen ob unsere Services laufen sollen ein [Monitoring](https://de.wikipedia.org/wiki/Monitoring) eingeführt werden.

### Auftrag

Erstellt einen neuen Server (VM) welche via Apache/CGI/REST Meldungen empfangen kann und diese lokal speichert.

Erstellt einen `cron` Job welche laufend die Services, z.B. 1mal pro Minute, überprüft und Meldungen an den neuen Server schickt.

**Zusatzauftrag**: installiert auf dem neuen Server ein Private Key und auf den bestehenden den Public Key, damit vom neuen Server ohne Password auf die alten Server gewechselt werden kann. Damit kann z.B. der neue Server die anderen Heruntergefahren.

**Es dürfen bestehende CGI Scripte wiederverwendet werden!**

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `100-SrvKonfiguration` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

*bitte Ausfüllen*