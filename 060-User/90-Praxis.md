Praxis
------

Um Unterhaltsarbeiten durchzuführen, sollt nicht mir root sondern mit dem eigenen Usernamen auf den VM's gearbeitet werden können.

### Auftrag

Legt für alle, welche Zugriff auf die Server haben sollen einen User auf den verschiedenen VM's an.

### Lösungsansatz

Es wird für jeden Administrator ein Account mit Usernamen angelegt. 

Der Administrator erhält ebenfalls das Recht mittels `sudo` root zu werden.

### Vorgehen - neue Gruppen und User in VM

Recherchiert im Internet welche Befehle zum Anlegen eines User nötig sind, z.B. via `ubuntu add user`.


Legt in den VM's eine eigene Gruppe und User an. Kopiert dazu das MultiMachine Beispiel im Verzeichnis `050-TCPIP/mm/` und Erweitert Vagrantfile um die nötigen Befehle. 


Die neuen User können wie folgt getestet werden:

* in der VM


	sudo -i
	su - admin01
	
* vom lokalen Host


	ssh admin01@localhost -p 2200


Der Port entspricht dem gemappten `ssh` Port pro VM, welche in Virtualbox unter Ändern -> Netzwerk -> Port-Weiterleitung nachgeschaut werden kann.

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `060-User` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**


### Vorgehen - Vereinfachung cd `Verzeichnis`

* Erweitert die .bashrc von CygWin um eine CDPATH Variable, damit einfach in das Repository Praxis Verzeichnis gewechselt werden kann, z.B. `cd 050-TCPIP`. 

	set CDPATH=$CDPATH:C:/TBZ/M300

* Erstellt einen Alias für Vagrant, z.B. `vg`

	alias vg=vagrant


### Lieferobjekte

VM's wo alle Administratoren mittels Usernamen erfasst sind. 

### Zusatzauftrag (optional)

Erstellt für alle Kameraden ein Login. Als Input dient ein Export der Klassendatei mit den Ablageorten aus Microsoft Office 365.

Die Klassendatei ist wie folgt aufbereitet und muss im CSV Format gespeichert sein:

	Name      Vorname   Ablageort
	Muster    Hans      git@gitlab.com:hans.muster/M300.git
	
