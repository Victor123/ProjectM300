Reflexion / Dokumentation
-------------------------

Ein effektives Überwachen und Protokollieren laufender Container ist ausgesprochen wichtig, wenn Sie ein nichttriviales System am Laufen halten und Probleme
effektiv debuggen wollen. 

Docker bringt  Standard Logging und Monitoring mit welches erweitert werden kann.

### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Warum sollten Container überwacht werden?

	 
	 	
	 	
	.......................................................................................

* Was ist das syslog und wo ist es zu finden?

	 
	 	
	 	
	.......................................................................................
	
* Was ist stdout, stderr, stdin?

	 
	 	
	 	
	.......................................................................................