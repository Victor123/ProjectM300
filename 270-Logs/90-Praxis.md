Praxis
------

Um Fehler besser analysieren zu können, sollen alle Docker Ausgaben in das syslog des Hosts geschrieben werden.

Zusätzlich soll mit [cAdvisor](https://github.com/google/cadvisor) das Monitoring der Docker Container eingeführt werden.

### Auftrag

Stellt sicher, dass alle Docker Container die Logging Ausgaben auf das syslog des Host weiterleiten.

Installiert die Monitoring Lösung [cAdvisor](https://github.com/google/cadvisor).

**Zusatzaufgabe**: Erweitert Jenkins um einen Jobs welche das syslog auswertet und evtl. Fehler anzeigt.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `270-Logs` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

Einheitliches Logging und Monitoring für alle Docker Container.