Reflexion / Dokumentation
-------------------------

"Infrastructure as Code" ist ein [Paradigma](https://de.wikipedia.org/wiki/Paradigma) zur Infrastruktur-Automation basierend auf Best Practices von der Softwareentwicklung. 


Im Vordergrund stehen konsistente und wiederholbare Definitionen für die Bereitstellung von Systemen und deren Konfiguration.


Die Definitionen werden in Dateien zusammengefasst, gründlich Überprüft und automatisch ausgerollt.

### Beispiele

* [Einfacher Webserver](web/README.md)

### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Was ist der Unterschied zur manuellen Installation der VM
	
	 
	 	
	 	
	.......................................................................................

* Was können wir Verbessern, Erweitern?

	 
	 	
	 	
	.......................................................................................

* Was fehlt, Ausgehend von unseren Grundsätzen?

	 
	 	
	 	
	.......................................................................................

