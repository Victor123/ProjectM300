Beispiel: einfacher Webserver
-----------------------------

Einfacher Webserver mit Ubuntu 16.x.


Die Dateien werden lokal im aktuellen Verzeichnis abgelegt: 

	config.vm.synced_folder ".", "/var/www/html"


Der Webserver ist unter [http://localhost:8080](http://localhost:8080) erreichbar.

    config.vm.network "forwarded_port", guest:80, host:8080, auto_correct: true
	 
Falls dieser belegt ist, wird automatisch eine Alternative gewählt.


**Starten mittels**

	vagrant up
	

