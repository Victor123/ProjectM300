﻿Praxis
------

Die anderen Entwickler haben Gefallen gefunden an der Entwicklungsmaschine für das "Internet der Dinge" und wollen ebenfalls so eine Umgebung.

### Auftrag

Erstellt mit wenig möglichst Aufwand für die x Entwickler eine Entwicklungsmaschine für das "Internet der Dinge"

Weitere Entwickler sollen diese Entwicklungsmaschine, selber aufsetzen können. 

Die Maschinen sollen alle gleich zu verwenden sein und bei Bedarf, durch eine neue Version ersetzt werden können. 

Zum Ausprobieren beschränken wir uns zuerst auf eine Lauffähige Version des Web Server Apache.

### Lösungsansatz

Statt jede Maschine einzeln aufzusetzen, wird der "Infrastructure as Code" Ansatz (Paradigma) gewählt.

Nach einer Grundinstallation von VirtualBox und Vagrant können die Entwickler neue Maschinen selber "on demand" aufsetzen.

In einem zweiten Schritt wird der "Code" in einem zentralen Repository abgelegt, wo alle Entwickler Zugriff haben und so selber
neue Versionen der Umgebung aufsetzen können.

### Vorgehen
	
* Installiert die Virtualisierung-Umgebung [VirtualBox](https://www.virtualbox.org/) auf dem Rechner.


* Installiert [vagrant](https://www.vagrantup.com/downloads.html).


* **Nur Windows**: Installiert ein ssh Client, z.B. integriert in [Git](https://git-scm.com/), [cygwin](https://www.cygwin.com/)


* Erstellt ein leeres Verzeichnis, z.B. `C:\TBZ\M300-Praxis\020-IaC\web`.


* Started ein Terminal: **Windows** Cygwin64 Terminal, **Mac** und **Linux**: Terminal


* Wechselt in das erstellte Verzeichnis und gebt folgende Befehle ein:


  * **Wenn ein interner Server vorhanden ist:**
 
 
    vagrant box add {{config.server}}/vagrant/ubuntu/xenial64.box --name ubuntu/xenial64
    cd C:/TBZ/M300-Praxis/020-IaC/web
    vagrant init ubuntu/xenial64
    vagrant up
 
  
  * alle anderen:
  
  
    cd C:/TBZ/M300-Praxis/020-IaC/web
    vagrant init ubuntu/xenial64
    vagrant up --provider virtualbox

	
* Die Befehle erzeugen eine neue Virtual Machine mit Ubuntu und eine Datei mit Namen Vagrantfile.


* Ergänzend/Ersetzt den Inhalt in Vagrantfile wie folgt, Zeilen mit # sind Kommentare und können ignoriert werden:


    Vagrant.configure(2) do |config|
      config.vm.box = "ubuntu/xenial64"
      config.vm.network "forwarded_port", guest:80, host:8080, auto_correct: true
      config.vm.synced_folder ".", "/var/www/html"  
    config.vm.provider "virtualbox" do |vb|
      vb.memory = "512"  
    end
    config.vm.provision "shell", inline: <<-SHELL 
      sudo apt-get update
      sudo apt-get -y install apache2
    SHELL
    end


* Editiert die neu erstellte Datei index.html und Ändert den Eintrag im ersten &lt;span&gt; hinter &lt;body&gt; auf einen beliebigen Text.


* Zerstört und Erstellt die Virtual Machine mittels:


    vagrant destroy -f
    vagrant up


* Startet den Browser und ruft [http://localhost:8080](http://localhost:8080) auf.


* **Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `020-Iac` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie.**

### Lieferobjekt

Wiederholbare und Nachvollziehbare Entwicklungsmaschine für das "Internet der Dinge" mit Ubuntu Desktop Linux, welche parallel neben Windows verwendet werden kann.

Aufsetzen der Entwicklungsmaschine wird an die Entwickler delegiert.

Die Wartung verbleibt bei der System Administration.

### Hinweis(e)

Die Virtuellen Maschinen werden unter C:\\Users\\`user`\\VirtualBox VMs abgelegt.

Im Vagrant Verzeichnis befindet sich die `Vagrantfile` und ein `.vagrant` Verzeichnis mit dem Verweis auf die VM.

README.md und 90-Praxis.md sind im [Markdown](https://de.wikipedia.org/wiki/Markdown) Format und können mit diversen Editoren und
Entwicklungsumgebungen wie [Eclipse](http://wiki.eclipse.org/Mylyn/User_Guide), [Visual Studio Code](https://code.visualstudio.com/), [GitBook](https://www.gitbook.com/) bearbeitet werden.

Ein fertiges Beispiel findet Ihr im `020-IaC/web` Verzeichnis.



	
