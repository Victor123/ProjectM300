Kontinuierliche Integration, Testen
-----------------------------------

[Kontinuierliche Integration](https://de.wikipedia.org/wiki/Kontinuierliche_Integration) (auch fortlaufende oder permanente Integration; englisch continuous integration) ist ein Begriff aus der Software-Entwicklung, der den Prozess des fortlaufenden Zusammenfügens von Komponenten zu einer Anwendung beschreibt. 

Ein [Modultest](https://de.wikipedia.org/wiki/Modultest) (auch Komponententest oder oft vom engl. unit test als Unittest bezeichnet) wird in der Softwareentwicklung angewendet, um die funktionalen Einzelteile (Module) von Computerprogrammen zu testen, d. h., sie auf korrekte Funktionalität zu prüfen.

Jenkins ist ein beliebter Open-Source-CI-Server.

### Beispiele

* [VM mit Docker und Jenkins](jenkins/README.md)

