Praxis
------

Mittels [Kontinuierlicher Integration](https://de.wikipedia.org/wiki/Kontinuierliche_Integration) und
[Modultests](https://de.wikipedia.org/wiki/Modultest) sollen die Docker Images einer laufenden Kontrolle unterzogen werden.

Damit sollen Änderungen in Paketquellen, Grundimages etc. frühzeitig aufgespürt, angezeigt und Fehler behoben werden können, bevor es der Kunde überhaupt bemerkt.

### Auftrag

Baut Jenkins Jobs die täglich die Docker Images builden und evtl. Fehler anzeigen.

Erweitert das Builden der Docker Images um Modultest, welche die Richtigkeit der Daten in den Images prüfen. Baut dazu Bash Scripts welche
mittels `run -d --rm ubuntu:14.04 <Bash Script` ausgeführt werden können.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `260-CITest` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**


### Lieferobjekte

Ausbaufähige Umgebung für [Kontinuierliche Integration](https://de.wikipedia.org/wiki/Kontinuierliche_Integration) und [Modultests](https://de.wikipedia.org/wiki/Modultest).

### Hinweise

Die Jenkins Umgebung des IoTKits zeigt wie man Jenkins Jobs via [Dockerfile](https://github.com/mc-b/IoTKit/tree/master/docker/jenkins) anlegt.
