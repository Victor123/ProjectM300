Praxis
------

Um einen Wildwuchs bei der Verwendung von Docker Images zu vermeiden, wird eine interne Registry eingerichtet.

Docker Images dürfen nur dort abgelegt und/oder geholt werden. 

### Auftrag

Erstellt eine VM mit einer eigenen Docker Registry, basierend auf Vagrant und Docker.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `250-Hub` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**


### Lieferobjekte

Interne Docker Registry.