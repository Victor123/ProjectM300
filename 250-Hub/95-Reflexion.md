﻿Reflexion / Dokumentation
-------------------------

Die einfachste Möglichkeit, Ihre Images bereitzustellen, ist der Einsatz des Dockers Hub. 

Bei diesem handelt es sich um die von Docker Inc. angebotene Online-Registry. 

Alternativ können Container mittels `docker export` und `docker import` und Images mittels `docker save` und `docker load` von/nach Dateien kopiert werden.

Drittens besteht die Möglichkeit eine eigene Registry zu verwenden.

### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Was ist der Unterschied zwischen der Verwendung der Docker Registry und `docker save` und `docker load`?

	 
	 	
	 	
	.......................................................................................

* Was ist der Unterschied zwischen `docker save`/`docker load` und `docker export`/`docker import`?

	 
	 	
	 	
	.......................................................................................


* Was ist bei einer eigenen Registry zu beachten?

	 
	 	
	 	
	.......................................................................................

* Warum sollten Versionen `tag` von Images immer angegeben werden?

	 
	 	
	 	
	.......................................................................................