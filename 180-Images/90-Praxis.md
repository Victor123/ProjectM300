﻿Praxis
------

Um die Arbeit mit Vagrant Boxen zu vereinfachen und einen Wildwuchs von eingesetzten Boxen entgegenzuwirken, wird eine oder zwei
Standard Boxen erstellt, von welcher wir unsere Intern Verwendeten Boxen jeweils ableiten.

### Auftrag

Erstellt eine oder zwei Standard Vagrant Box welche als Grundlage für weitere Interne Boxen dient.

Erhebt dazu die internen Anforderungen und notiert die benötigte Software und Dienste.

### Lösungsansatz

Nach einer Erhebung der am meisten benötigten Software und Diensten erstellen wir eine oder zwei Vagrant Boxen welche die
meisten Anforderungen abdecken.

### Vorgehen

#### Packer

Downloadet das [IoT Kit] Beispiel]() wechselt in das Verzeichnis IoTKit/docker/packer.

Entfernt die Einträge für docker aus der Datei `ubuntu-server.json` und startet Packer build.

	packer build ubuntu-server.json
	
#### Sharen von Boxen

Erstellt einen Account auf [https://atlas.hashicorp.com/vagrant](https://atlas.hashicorp.com/vagrant) und trägt 
die erstellte Box dort ein.

#### Manuelle Installation

Installiert ein Betriebssystem der Wahl, Exportiert es nach Vagrant Box und stellt es bei [https://atlas.hashicorp.com/vagrant](https://atlas.hashicorp.com/vagrant) ein.

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `180-Images` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

Ein oder zwei Vagrant Boxen welche aus Grundlage für unsere Interne Infrastruktur dienen.
