Reflexion / Dokumentation
-------------------------

[Packer](https://www.packer.io/) ist ein Tool zur Erstellung von Images / Boxen für eine Vielzahl von 
Dynamic Infrastructure Platforms mittels einer Konfigurationsdatei.

Neue Vagrant Boxen können, mittels Internet, geteilt werden.

Alternativ zu Packer können Vagrant Boxen aus normal Installierten VM erstellt werden.
