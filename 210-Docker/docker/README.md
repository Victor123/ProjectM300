Ubuntu/Docker mit MySQL
-----------------------

Ubuntu/Docker VM mit MySQL Docker Image zu Testzwecken.

Image builden:

	cd /vagrant/mysql
	docker build -t mysql .
	
starten:

	docker run --rm -d --name mysql mysql

Funktionsfähigkeit überprüfen:

	docker exec -it mysql bash
	
und im Container

	ps -ef
	netstat -tulpen

MySQL Client im Container starten:

	docker exec -it mysql mysql -uroot -pS3cr3tp4ssw0rd