Praxis
------

Als Prototype wollen wir zuerst die MySQL Datenbank nach Docker überführen.

### Auftrag

* Baut eine VM mit einem MySQL Docker Image.

### Lösungsansatz

*bitte Ausfüllen*

### Vorgehen

*bitte Ausfüllen*

**Dokumentiert das gelernte, dafür stehen Vorbereitete Dokumente README.md und 90-Praxis.md im Verzeichnis** `210-Docker` **zur Verfügung. Kopiert diese in Euren Praxis Ordner und ergänzt sie. Checkt alle neuen Dokumente im Repository ein.**

### Lieferobjekte

Ein neue VM mit einem Lauffähigem MySQL Docker Container.