Reflexion / Dokumentation
-------------------------

Docker nahm die bestehende Linux-Containertechnologie auf und verpackte und erweiterte sie in vielerlei Hinsicht – vor allem durch portable Images und eine benutzerfreundliche Schnittstelle –, um eine vollständige Lösung für das Erstellen und Verteilen von Containern zu schaffen. 

Die Docker-Plattform besteht vereinfacht gesagt aus zwei getrennten Komponenten: der Docker Engine, die für das Erstellen und Ausführen von Containern verantwortlich ist, sowie dem Docker Hub, einem Cloud Service, um Container-Images zu verteilen.

### Präsentation

![](../../images/Reflexion.png)

### Fragen

* Was ist der Unterschied zwischen einem Docker Image und einem Container?

	 
	 	
	 	
	.......................................................................................

* Was ist der Unterschied zwischen einer Virtuellen Maschine und einem Docker Container?

	 
	 	
	 	
	.......................................................................................
	
* Wie bekomme ich Informationen zu einem laufenden Docker Container?

	 
	 	
	 	
	.......................................................................................